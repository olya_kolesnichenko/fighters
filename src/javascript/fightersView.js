import View from './view';
import FighterView from './fighterView';
import FighterDetailsView from './fighterDetailsView';
import {fighterService} from './services/fightersService';

class FightersView extends View {
    constructor(fighters) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.handleSaveData = this.handleSaveData.bind(this);
        this.createFighters(fighters);
    }

    fightersDetailsMap = new Map();

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter) {
        let fighterData;
        if (!this.fightersDetailsMap.has(fighter._id)) {
            fighterData = await fighterService.getFighterDetails(fighter._id);
            this.fightersDetailsMap.set(fighter._id, fighterData);
        } else {
            fighterData = this.fightersDetailsMap.get(fighter._id);
        }

        const fighterDataModal = new FighterDetailsView(fighterData, this.handleSaveData);
        this.element.append(fighterDataModal.element);

    }

    async handleSaveData(event, fighter, elem) {
        const health = document.getElementById('health').value;
        const attack = document.getElementById('attack').value;
        const defense = document.getElementById('defense').value;
        this.fightersDetailsMap.set(fighter._id, {...fighter, health, attack, defense});
        elem.parentElement.removeChild(elem);
    }
}

export default FightersView;