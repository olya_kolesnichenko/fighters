import View from './view';

class FighterDetailsView extends View {
    constructor(fighter, handleSaveData) {
        super();

        this.createFighterModal(fighter, handleSaveData);

        this.handleRemoveModal = this.handleRemoveModal.bind(this);
        this.handleChangeProperty = this.handleChangeProperty.bind(this);

    }

    createFighterModal(fighter, handleSaveData) {
        const {name, health, attack, defense} = fighter;
        const nameElement = this.createName(name);
        const healthElement = this.createPropertyEditor('health', health);
        const attackElement = this.createPropertyEditor('attack', attack);
        const defenseElement = this.createPropertyEditor('defense', defense);
        const closeElement = this.createElement({tagName: 'span', className: 'close'});
        const saveButton = this.createElement({tagName: 'button', className: 'save'});
        const contentElement = this.createElement({tagName: 'div', className: 'modal-content'});

        closeElement.innerText = 'X';
        saveButton.innerText = 'Save';
        this.element = this.createElement({tagName: 'div', className: 'modal'});
        contentElement.append(closeElement, nameElement, healthElement, attackElement, defenseElement, saveButton);
        this.element.append(contentElement);
        closeElement.addEventListener("click", event => this.handleRemoveModal(event, this.element));
        saveButton.addEventListener("click", event => handleSaveData(event, fighter, this.element));
    }

    createName(name) {
        const nameElement = this.createElement({tagName: 'span', className: 'name'});
        nameElement.innerText = name;
        return nameElement;
    }

    createPropertyEditor(property, data) {
        const propertyElement = this.createElement({tagName: 'div', className: 'property' });
        const valueElement = this.createElement({tagName: 'input', className: 'value'});
        const incrementButton = this.createElement({tagName: 'button', className: 'button-sign'});
        const decrementButton = this.createElement({tagName: 'button', className: 'button-sign'});
        valueElement.id = property;
        valueElement.setAttribute("type", "number");
        valueElement.setAttribute("min", "0");
        valueElement.setAttribute("readonly", true);
        incrementButton.innerText = "+";
        decrementButton.innerText = "-";
        incrementButton.addEventListener("click", event => this.handleChangeProperty(event));
        decrementButton.addEventListener("click", event => this.handleChangeProperty(event));
        valueElement.value = data;
        propertyElement.append(decrementButton, valueElement, incrementButton);
        return propertyElement;
    }

    handleRemoveModal(event, elem) {
        elem.parentElement.removeChild(elem);
    };

    handleChangeProperty(event) {
        console.log(event.target.innerText);
        const inputProperty = event.target.parentElement.getElementsByTagName('input')[0];
        if (event.target.innerText === '-'){
            if (parseInt(inputProperty.value) > inputProperty.getAttribute("min")){
                inputProperty.value--;
            }
        } else {
            inputProperty.value++;
        }

    };
}





export default FighterDetailsView;